#include <stdio.h>
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include "cpp_brent.h"
using namespace std;

//g++ -O0 -g -std=c++11 Brent1.cpp -o Brent1


fstream file3;
int start=0;



double f1(double x, double y1, double y2){
	return -y1*y1-y2;
}

double f2(double x, double y1, double y2){
	return 5*y1-y2;
}

double rk4(double (*f)(double,double,double), double (*m)(double,double,double), double x, double h, double *y1, double *y2) {

	double k1,k2,k3,k4,m1,m2,m3,m4;
	k1 = h*f(x,*y1,*y2);
	m1 = h*m(x,*y1,*y2);
	k2 = h*f(x+0.5*h,*y1+0.5*k1,*y2+0.5*m1);
	m2 = h*m(x+0.5*h,*y1+0.5*k1,*y2+0.5*m1);
	k3 = h*f(x+0.5*h,*y1+0.5*k2,*y2+0.5*m2);
	m3 = h*m(x+0.5*h,*y1+0.5*k2,*y2+0.5*m2);
	k4 = h*f(x+h,*y1+k3,*y2+m3);
	m4 = h*m(x+h,*y1+k3,*y2+m3);
	*y1 = *y1+(k1+2.*k2+2.*k3+k4)/6.;
	*y2 = *y2+(m1+2.*m2+2.*m3+m4)/6.; 
}


double RK45(double y2){

	double x = 0.;
	double xfin = 10.;
	//double x=xin;
	double err=1.e-6;
	double h=1.; //initial stepsize
	file3.open("Breantq1.txt",ios::out);


	double y1;
	double k1,k2,k3,k4;
	double y1a,y1b,y2a,y2b;
	double delta1=1.0, delta2=1.0;
	double delta, h2;

	y1 = 1.5;
	// y2 = x;
	file3<<"value of x "<<"value of y1 "<<"value of y2 "<<endl;
	file3<<x<<"  ,  "<<y1<<"  ,  "<<y2<<endl;
	while(x<xfin){
		delta1=1.0;
		delta2=1.0;

		y1a = y1b = y1;
		y2a = y2b = y2;

		// RK with full step
		rk4(f1, f2, x, h, &y1a, &y2a);

		// RK4 with double step
		h2 = h/2.;
		rk4(f1, f2, x, h2, &y1b, &y2b);
		rk4(f1, f2, x+h, h2, &y1b, &y2b);

		// Calculate delta
		delta1=fabs(double (y1a-y1b));
		delta2=fabs(double (y2a-y2b));

		// Check if converges
		if((delta1 > err) && (delta2 > err)) {
			h = h/2.;
		}else{
			y1 = y1b+delta1/15.;
			y2 = y2b+delta2/15.;
			x=x+h; //increment of x

			// Check if is slow
			delta = max(delta1, delta2);
                        if (delta < 0.1*err)
				h = 2.*h;
            	
			
			// CHeck if bigger than 10
                        if ((x+h) > 10.0) {
                              h = 10.0 - x;
			}
			file3<<x<<"\t"<<y1<<"\t"<<y2<<endl;
			start++;
		}
	}
	file3<<"number of points used is:"<<start<<endl;
	file3.close();
	return y1;
}

int main() {
	double x, y1b, y2b, f1, f2, m, h, y1, y2;
	// Example function to find root of:
	auto w = [](double x) {
		double y =  RK45(x);
		return y; 
	};
	double a = 0.0;  // lower bound of search interval
	double b = -10.;     // upper bound of search interval
	double tolerance = 0.0001;  // desired solution tolerance
	double max_iterations = 100;

	double root = brents_method(w, a, b, tolerance, max_iterations);

	cout << "Root is: " << root << endl;  // produces 'root' of rk45

	file3<<x<<"  ,  "<<y1<<"  ,  "<<y2<<endl;
}
