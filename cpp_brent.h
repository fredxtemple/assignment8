#ifndef CPP_BRENT_H_
#define CPP_BRENT_H_

#include <cmath>
#include <functional>

// brents_method 1:
//
// This is the first method of using the function. As well as specifying
// the funcion f, and the lower and upper bounds, one should give the
// tolerance and the max iterations as arguments.
inline double brents_method(std::function<double (double)> f,
                     double lower_bound,
                     double upper_bound,
                     double tolerance,
                     double max_iterations);
// brents_method 2:
//
// Using this function, the tolerance is set to a default value of 0.0001,
// and the max iterations to 500.
inline double brents_method(std::function<double (double)> f,
                     double lower_bound,
                     double upper_bound) {
    return brents_method(f, lower_bound, upper_bound, 0.0001, 500);
}
inline double brents_method(std::function<double (double)> f,
                     double lower_bound,
                     double upper_bound,
                     double tolerance,
                     double max_iterations) {
    double a = lower_bound;
    double b = upper_bound;
    double fa = f(a);
    double fb = f(b);
    double fs = 0;

    if ((fa * fb) >= 0) {
        throw std::runtime_error("Brents method bad bounding");
    }

    if ( std::abs(fa) < std::abs(fb) ) {
        double swp = a;
        double swpf = fa;
        a = b;
        fa = fb;
        b = swp;
        fb = swpf;
    }

    double c = a;
    double fc = fa;
    bool mflag = true;
    unsigned int it = 0;
    double s = 0;
    double d = 0;

    while ( std::abs(b - a) > tolerance) {
        if (fa != fc && fb != fc) {
            double s1 =  a * fb * fc / ((fa - fb) * (fa - fc));
            double s2 =  b * fa * fc / ((fb - fa) * (fb - fc));
            double s3 =  c * fa * fb / ((fc - fa) * (fc - fb));
            s = s1 + s2 + s3;

        } else {
            s = b - fb * (b - a) / (fb - fa);
        }

        if ( ((s < (3 * a + b) / 4) || (s > b))    ||
             (mflag && (std::abs(s-b) >= (std::abs(b-c)/2))) ||
             (!mflag && (std::abs(s-b) >= (std::abs(c-d)/2))) ||
             (mflag && (std::abs(b-c) < tolerance)) ||
             (!mflag && (std::abs(c-d) < tolerance)) ) {
            s = (a + b) / 2;
            mflag = true;
        } else {
            mflag = false;
        }

        fs = f(s);
        d = c;
        c = b;
        fc = fb;

        if (fa * fs < 0) {
            b = s;
            fb = fs;
        } else {
            a = s;
            fa = fs;
        }

        if ( std::abs(fa) < std::abs(fb) ) {
            double swp = a;
            double swpf = fa;
            a = b;
            fa = fb;

            b = swp;
            fb = swpf;
        }

        it += 1;
        if (it >= max_iterations) {
            throw std::runtime_error("Not converged in iteration limit");
        }
    }
    return s;
}
#endif  // CPP_BRENT_H_